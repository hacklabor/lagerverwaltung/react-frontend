# Warehouse Frontend

Es bleiben Bei Projektarbeiten doch immer Bauteile übrig. Warum diese nicht mit allen unkompliziert Teilen. Dafür haben wir das "Warehouse" geschaffen.Das Lagersystem dient zum einlagern und auslagern von elektronischen Bauteilen von Mitgliedern. Hintergrund ist das man schnell Projekte umsetzen kann ohne erst lange auf Teile warten zu müssen die "Lübecker" nennen sowas strategische Reserve https://www.youtube.com/watch?v=Drcc3lcdbbM. Auf wird durch das physische vorhandensein die Ideen beflügelt um Projekte möglich zu machen.

Facts

- die Bauteile stammen von den Mitgliedern des Vereins und sind deren Eigentum
- Jeder der einen Account hat kann Bauteile Ausleihen und muss diese inerhalb von 8  China Wochen ersetzen.
- Der Verein bekommt auch einen Account jeder kann darüber Bauteile dem Verein spenden
- generell ist ein "Kaufen" nicht vorgesehen. Es steht nartürlich jedem Mitglied frei aus seinem Kontigent Bauteile zu veräussern
- Bedienung über den Pi am Lager mit RFID Card Leser und QR Code Scanner aber auch über jedes Handy/Notebook 

# zu erreichen unter https://warehouse01.hacklabor.de

## Server Update

sudo ./update.sh


## Hauptbildschirm "itemList"

![Drawing](Images/Main.JPG "")


- entweder mit suche Bauteile die Auswahl eingrenzen
- ansonsten click auf das Bild öffnet das Detail Fenster des Items

![Drawing](Images/Suche.JPG "")


## Item Detail
![Drawing](Images/detail.JPG "")

- Hier werden die Boxen angezeigt in welchen das Bauteil eingelagert ist
- Ein Bauteil kann in meheren Boxen liegen und eine Box kann verschieden Bauteile enthalten
- ein klick auf die Box (UnitID) Zeigt den aktulelen Standort der Box im Regal mit den LEDs an
- Die Anzahl ist immer die Gesamt im Lager verfügbare Anzahl

## Item Detail angemeldet
![Drawing](Images/detail_Angemeldet.JPG "")
- Nachdem man sich angemeldet hat (RFID Leser) oder Eingabe seiner 10 setlligen RFID ID sind mehere neue Funktionen verfügbar

## Bearbeiten
![Drawing](Images/Bearbeiten.JPG "")
- hier ist es möglich die Daten des Bauteils zu ändern
- mit "Save" wird der Datensatz gespeichert
- mit "Neu" erstellt man ein komplett neuen Bauteildatensatz

## Unit Zuordnen 
![Drawing](Images/UnitZuordnen.JPG "")
- Eingabe der Unit ID in welches man das Bauteil einlagern will
- Ich kann aber auch den QR Code der Unit scannen wenn ich mich im dem Item Detail Fenster befinde

## Unit Zuordnung löschen 
-geht noch nicht Backend arbeiten Notwendig


## Ein/Auslagern eines Bauteiles

- Anmelden
- Bauteil auswählen
- Unit (Box) auswählen
- mit den Plus und Minus Tasten die entsprechende Anzahl der Bauteile EIN=PLus oder Aus=minus lagern
- alternativ geht das auch mit den auf dem Kabelkanal aufgeklebten +10 +1 -1 -10 QR Code
- Es wird noch einen weiteren Button geben "spende für den Verein" dann kann man Bauteile zur allgemeinen Vereinsverfügung einlagern
![Drawing](Images/UNITSelected.JPG "")

## Items eines User's
- Klick auf den Benutzernamen öffnet die Seite des Benutzers
- Hier kann man sehen wieviel Bauteile man noch wiederbeschaffen muss bzw wieviel Bauteil man noch auf Lager hat.

![Drawing](Images/User.JPG "")

## Zuordnung der BOxen (Units) zum Regalplatz (Shelf)
- nachdem man den QR Code einer Box gescannt hat hat man 60sec Zeit ein Lagerplatz QR Code zu Scannen
- Dadurch wird der Box der neue Lagerplatz zugeordned


## Wissen was in einer Box ist

- scannt man den Qr Code einer Box und man ist nicht im "Detail" Bildschirm wird Die Box und deren Ihnalt angezeigt. Bedenke es können auch mehere unterschiedliche Bauteile in einer Box sein

![Drawing](Images/GetUnit.JPG "")






