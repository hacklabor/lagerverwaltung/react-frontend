import { combineReducers } from "redux";
import axios from "./axiosReducer";
import UserData from "./userReducer"
import AppData from "./appReducer"
import ScanData from "./scanReducer"
import MQTT from "./mqttReducer"

export default combineReducers({
    axios,
    UserData,
    AppData,
    ScanData,
    MQTT
});

