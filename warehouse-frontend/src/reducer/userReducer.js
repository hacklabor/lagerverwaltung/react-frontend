

const initialState = {
    UserData: [],
    login: false,
    loginTime: new Date().getTime()

};

export default function UserReducer(state = initialState, action) {
    switch(action.type) {
        case 'USER_DATA_LOGIN':
            // Mark the state as "loading" so we can show a spinner or something
            // Also, reset any errors. We're starting fresh.
            return {
                ...state,
                UserData: action.payloadUser,
                login: action.payloadLogin,
                loginTime: new Date().getTime()
            };
        case 'USER_DATA_LOGOUT':
            // Mark the state as "loading" so we can show a spinner or something
            // Also, reset any errors. We're starting fresh.
            return {
                ...state,
                UserData: [],
                login: false,

            };
        default:
            // ALWAYS have a default case in a reducer
            return state;
    }
}