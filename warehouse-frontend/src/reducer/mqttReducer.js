

const initialState = {
    payload: [],
    sendData: false,


};

export default function mqttReducer(state = initialState, action) {
    switch(action.type) {
        case 'MQTT_SEND_PAYLOAD':
            // Mark the state as "loading" so we can show a spinner or something
            // Also, reset any errors. We're starting fresh.
            return {
                ...state,
                sendData: action.sendData,
                payload: action.payload,

            };

        default:
            // ALWAYS have a default case in a reducer
            return state;
    }
}