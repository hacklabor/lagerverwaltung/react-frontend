

const initialState = {
    AppState: [],


};

export default function appReducer(state = initialState, action) {
    switch(action.type) {
        case 'APP_STATE':
            // Mark the state as "loading" so we can show a spinner or something
            // Also, reset any errors. We're starting fresh.
            return {
                ...state,
                AppState: action.payload
            };

        default:
            // ALWAYS have a default case in a reducer
            return state;
    }
}