

const initialState = {
    scanData: [],


};

export default function scanReducer(state = initialState, action) {
    switch(action.type) {
        case 'CHANGE_STATE':
            // Mark the state as "loading" so we can show a spinner or something
            // Also, reset any errors. We're starting fresh.
            return {
                ...state,
               scanData: action.payload,

            };

        default:
            // ALWAYS have a default case in a reducer
            return state;
    }
}