import axios from "axios";


import {server} from "../components/ProgrammSettings"
export const AXIOS_BEGIN   = 'AXIOS_BEGIN';
export const AXIOS_SUCCESS = 'AXIOS_SUCCESS';
export const AXIOS_FAILURE = 'AXIOS_FAILURE';

export const axiosBegin = () => ({
    type: AXIOS_BEGIN
});

export const axiosSuccess = products => ({
    type: AXIOS_SUCCESS,
    payload: { products }
});

export const axiosFailure = error => ({
    type: AXIOS_FAILURE,
    payload: { error }
});
export function Start() {
    console.log("START");

    return dispatch => {
        dispatch(axiosBegin());


    };
}

export function GetData(Address) {

    let url = server()+Address;
    console.log("Server",url);
    return dispatch => {
        dispatch(axiosBegin());
        axios.get(url)

            .then(function (response) {
                console.log(response);
                dispatch(axiosSuccess(response));
            })
            .catch(function (error) {
                console.log("GETError");
                console.log(error);
                dispatch(axiosFailure(error));
            });

    };
}

export function PostData(Address,payload) {
    let url = server()+Address;
    return dispatch => {
        dispatch(axiosBegin());
        axios({
            method: "post",
            url: url,
            data: payload
        })
            .then(function (response) {
                console.log(response);
                dispatch(axiosSuccess(response));
            })
            .catch(function (error) {
                console.log("GETError");
                console.log(error);
                dispatch(axiosFailure(error));
            });

    };
}

    export function PutData(Address,payload) {
        let url = server()+Address;
        return dispatch => {
            dispatch(axiosBegin());
            axios( {method: "put",
                url: url,
                data: payload
            })
                .then(function (response) {
                    console.log(response);
                    dispatch(axiosSuccess(response));
                })
                .catch(function (error) {
                    console.log("GETError");
                    console.log(error);
                    dispatch(axiosFailure(error));
                });

        };
}