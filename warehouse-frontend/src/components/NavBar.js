import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar'
import {connect} from "react-redux";
import {UserDataLogin, UserDataLogout} from '../action/userActions'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton';
import Typography from "@material-ui/core/Typography";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import logo from '../images/Logo_small_white.png';
import Notifier, { openSnackbar } from './Notifier';

import { useSelector} from 'react-redux'
import {Link} from "react-router-dom";
import TextField from "@material-ui/core/TextField";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    logo: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing(0),
        color: '#ffffff',
    },
    user: {
        textAlign: 'right',
        marginRight: theme.spacing(4),

    },
    usericon: {

    }
}));


const handleChangeRFID_ID =props=> event => {
    if (!event){return}
    console.log(event.target.value);
    let text = event.target.value;
    // 10 Zeichen vielleicht rfidID
    if (!text){return}

    console.log(text.length);
    if (text.length === 10) {
        const { dispatch } = props;
        let id1 = parseInt(text.substring(text.length - 10, text.length + 10));
        // Number?
        if (Number.isInteger(id1)) {

            dispatch(UserDataLogin({rfidChipId: id1},false));
            let {history} = props;
            history.push("/getUser/0");
        }
    }
};



const NavBar = (props) => {
    const User = useSelector(state => state.UserData);
    const classes = useStyles();
    const isLoggedIn = User.login;

    let history = props.history;



    const logoutButton = (isLoggedIn) => {
        const Logout = props => e => {
            const { dispatch } = props;

                dispatch(UserDataLogout(User));

                history.push('/0');
                openSnackbar({ message: 'Du wurdest abgemeldet' });

        };

        if (isLoggedIn) {
            return (
                <IconButton className={classes.button} aria-label="Logout" onClick={Logout(props)}>
                    <ExitToAppIcon style={{ fontSize: 30 }} />
                </IconButton>
            );
        }
    };

    const UserName = (isLoggedIn,props) =>{
        if(isLoggedIn) {
            return (
                <Link to="/getUser/0" style={{ textDecoration: 'none', color: 'white'  }}>
                 <Typography component="body1">
                     {User.UserData.userName}

                     <PersonIcon className={classes.usericon} style={{ fontSize: 30 }} />


                 </Typography>
                </Link>
            );
        }
        else {
            return (


                <TextField
                    style={{ padding: 5}}
                    id="RFID_ID"
                    placeholder="RFID Nr. für LOGIN"
                    margin="normal"
                    onChange={handleChangeRFID_ID(props)}
                />

            );
        }
    };







    return(
<div className={classes.root}>
<AppBar position="static">

    <Toolbar variant="regular" color="inherit">
        <Link to="/0" style={{ textDecoration: 'none' }}><img src={logo} width="40" height="40" alt="Hal" className={classes.logo} /></Link>
        <Typography variant="h6" component="h1" className={classes.title}>
         Warehouse Application
        </Typography>

    <Link to="/0" style={{ textDecoration: 'none' }}>
        <IconButton className={classes.button} aria-label="Home">
            <HomeIcon style={{ fontSize: 30 }} />
        </IconButton>
    </Link>

    { logoutButton(isLoggedIn) }


    </Toolbar>
    <div className={classes.user}>{ UserName(isLoggedIn,props) }</div>
</AppBar>
    <Notifier/>


</div>
    )
};

const mapStateToProps = state => (

    { retData: state.axios.items,
        loading: state.axios.loading,
        error: state.axios.error,
        lastURL: state.AppData.AppState.lastURL,
    }
);
//export default NavBar;
export default connect(mapStateToProps)(NavBar);