import React, {Component} from "react";

import {connect} from 'react-redux';

import Grid from "@material-ui/core/Grid";
import unitImage from "../images/unit.JPG"
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import {CardContent} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {server} from "./ProgrammSettings";
import axios from "axios";

import {mqttSend} from "../action/mqttActions";
import {UnitSize} from "./MqttSend";
import Item from "./Item";

// Or commonjs

class getUnit extends Component {
    getData = async (url) => {
        let res = await axios.get(url);
        let {data} = res.data;
        return await data
    };

    putData = async (url, payload) => {
        console.log("putData", url, payload);
        let res = await axios.put(url, payload);
        let {data} = res.data;
        return await data;
    };

    constructor(props) {
        super(props);
        this.state = {

            unitID: this.props.match.params.UnitID,
            UnitData: null,
            ShelfData: null,


        };

    }


    GetLocation() {
        console.log("GetLocation")

    }

    componentDidMount() {
        console.log("DidMount: getUnit", this.props);

        if (!this.state.UnitData) {
            (async () => {
                try {
                    this.setState({UnitData: await this.getData(server() + "storageUnits/" + this.props.match.params.UnitID)});
                } catch (e) {
                    alert("Unit mit Nummer: "+this.props.match.params.UnitID+" nicht vorhanden \n"+ e);//...handle the error...
                    this.props.history.push("/0")

                }
            })();
        }

    }


    render() {

        if (!this.state.UnitData) {
            return <div></div>;
        }

        const {login, history, dispatch, lastUnitID} = this.props;
        const {UnitID, ShelfNr, RowNr, ColNr} = this.props.match.params;


        let {UnitData} = this.state;
        console.log("UNITDATA", UnitData);
        if (parseInt(UnitData.id) !== parseInt(lastUnitID)) {
            if (UnitData.shelfId > 0 && UnitData.row > 0 && UnitData.column > 0) {
                let PiLED = {
                    "function": "location",
                    "row": UnitData.row - 1,
                    "shelf": UnitData.shelfId - 1,
                    "column": UnitData.column - 1,
                    "size": UnitSize(UnitData.id),
                };
                dispatch(mqttSend(true, PiLED));
            }
        }
        let i = 0;
        if (ShelfNr !== undefined && login === true) {


            let Id = [];
            for (i = 0; i < UnitData.items.length; i++) {
                Id.push(UnitData.items[i].id)

            }
            console.log(Id);
            let txData = {
                column: ColNr,
                itemIds: Id,
                row: RowNr,
                shelfId: ShelfNr
            };


            {
                (async () => {
                    try {
                        this.setState({ShelfData: await this.putData(server() + "storageUnits/" + this.props.match.params.UnitID, txData)});
                        history.push("/Blank");
                        history.push("/getUnit/" + UnitID)
                    } catch (e) {

                        alert(e);//...handle the error...
                        console.log(e)
                    }

                })();
            }


        }





       
        return (

            <div>{UnitData.items ? (
                <Card style={{maxWidth: '100%'}}>

                    <CardMedia
                        style={{height: 0, paddingTop: "30%"}}
                        image={unitImage}

                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h5">
                            Unit {UnitData.id}
                        </Typography>

                        <Typography variant="body2" color="textSecondary"
                                    component="p">Regal: {UnitData.shelfId} Reihe: {UnitData.row} Spalte: {UnitData.column} </Typography>

                    </CardContent>


                    <Grid container spacing={3} style={{padding: 3}}>


                        {UnitData.items.map((currentItem, id) => (
                            <Grid key={id} item xs={8} sm={3} lg={3} xl={2}>

                                <Item
                                    item={currentItem}
                                    isLogedIn={false}
                                    unitID={0}
                                    user={null}
                                    parrentProps={this.props}
                                    search={""}
                                    coins={0}
                                    large={false}
                                />
                            </Grid>
                        ))}


                    </Grid>

                </Card>

            ) : null}
            </div>
        );
    }


}

const mapStateToProps = state => (

    {
        login: state.UserData.login,
        UserID: state.UserData.UserData,
        lastUnitID: state.AppData.AppState.UnitID,
    }
);

export default connect(mapStateToProps)(getUnit)