import React, {useState} from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import TextField from "@material-ui/core/TextField";
import {makeStyles} from '@material-ui/core/styles';
import CreateIcon from '@material-ui/icons/Create';
import {red} from '@material-ui/core/colors';
import SaveAltRoundedIcon from '@material-ui/icons/SaveAltRounded';
import {Get, Post, Put} from "./ServerDataHandling";
import {Link, useHistory} from 'react-router-dom'
import {connect} from "react-redux";


const useStyles = makeStyles({
    root: {
        maxWidth: 500,

        margin: "auto",
    },
    media: {
        height: 500,
    },
    avatar: {
        avatar: {
            backgroundColor: red[500],
        },
    },
});


function Item(props) {


    const history = useHistory(props.item)
    const [item, setItem] = useState(props.item);
    const [user, setUser] = useState(props.user);
    const [dataStateUnit, setDataStateUnit] = useState(1);
    const [isLoggedIn, setIsLogedIn] = useState(props.isLogedIn);
    const [editMode, setEditMode] = useState(false);
    const [unitId, setUnitId] = useState(props.unitID);
    const {coins} = props
    const {large} = props
    const classes = useStyles();


    let myItemsTxt = ""
    let unitIdTxt = ""

    let availableCount = item.availableCount;
    let linkTo = "/"
    let i = 0;


    /*******************************************************************
     * Funktionen
     ******************************************************************/

    function handleChange(event) {
        const {id, value} = event.target;


        setItem((prevValue) => {
            return {
                ...prevValue,
                [id]: value,
            };
        });
    }

    function handleChangeUnitId(event) {
        let {value} = event.target;
        if (value < 0) {
            value = 0
        }
        setUnitId(value)
    }

    function save() {
        setEditMode(false);
        Put("items/" + item.id, item, "Put Data", "Daten gespeichert")
        setDataStateUnit(null)
    }

    //Create new Unit Trigger in fuction save()
    if (!dataStateUnit) {
        (async () => {
            try {
                setDataStateUnit(await Get("storageUnits/" + unitId, "GetUnit"));
                if (props.Beverage) {
                    history.push("/BeverageList/")
                }
                if (props.Electronic) {
                    history.push("/getItem/" + item.id + "/" + unitId)
                }
            } catch (e) {
                console.log(e)
                if (e.response.status === 404) {
                    await Post
                    ("storageUnits/",
                        {
                            "id": unitId,
                            "column": 0,
                            "row": 0,
                            "shelfId": 0
                        },
                        "New Unit",
                        "New Unit " + unitId + " wurde angelegt")
                    if (props.Beverage) {
                        history.push("/BeverageList/")
                    }
                    if (props.Electronic) {
                        history.push("/getItem/" + item.id + "/" + unitId)
                    }

                }
            }
        })();
    }


    /*******************************************************************
     * Rechnen
     ******************************************************************/

    // Anzahl der Item's
    if (item.storageUnits !== undefined) {
        availableCount = 0;
        if (item.storageUnits.length > 0) {
            for (i = 0; i < item.storageUnits.length; i++) {
                availableCount += item.storageUnits[i].availableCount
            }
        }
    }

    //Anzahl meiner Item's
    if (user && isLoggedIn) {
        let {inventory} = user;
        if (inventory[item.id] !== undefined) {
            if (props.Beverage) {
                availableCount = inventory[item.id].count
            }
            myItemsTxt = " meine : " + inventory[item.id].count
        }
    }

    if (unitId > 0) {
        unitIdTxt = " UnitId: " + props.unitID
    }


    /*********************************************************************************
     Style
     ********************************************************************************/
    const headerStyle = {backgroundColor: "#00adb5"};
    const contentStyle = {backgroundColor: "#393e46", color: "#eeeeee",};

    let avatarStyle = {
        color: "black",
        background: "red",
        width: 70,
        height: 70,
    };


    if (availableCount > 0) {
        avatarStyle = {...avatarStyle, background: "yellow",}
    }
    if (availableCount > 5) {
        avatarStyle = {...avatarStyle, background: "lime",}
    }

    if (props.Beverage) {
        linkTo = "/BeverageList/"
    }
    if (isLoggedIn && ((coins > 0) || (item.id === 1000000))) {


        if (user !== null) {

            if (user.rfidChipId !== undefined) {
                linkTo = "/PayBeverage/" + user.rfidChipId + "/" + item.id
            }
        }
    }
    if (props.Electronic) {
        linkTo = "/getItem/" + item.id + "/0"
    }


    /*********************************************************************************
     Rendern
     ********************************************************************************/


    return (
        <div>
            <Card className={classes.root}>
                < Link to={linkTo} style={{textDecoration: "none"}}>
                    <CardActionArea>
                        <CardHeader
                            style={headerStyle}
                            avatar={
                                <Avatar aria-label="recipe" style={avatarStyle}>
                                    {availableCount}
                                </Avatar>
                            }
                            title={item.name}
                            subheader=
                                {
                                    "ItemId: " + item.id +
                                    unitIdTxt +
                                    myItemsTxt

                                }
                        />
                        <CardMedia
                            className={classes.media}
                            image={item.imageUrl}
                            title={item.name}
                            avatar={
                                <Avatar aria-label="recipe" style={avatarStyle}>
                                    {props.count}
                                </Avatar>
                            }
                        />
                    </CardActionArea>
                </Link>
                {large ? (
                    <div>
                        {!editMode ? (<CardContent style={contentStyle}>

                                    <Typography gutterBottom variant="h5" component="h2">
                                        {item.name}
                                    </Typography>
                                    <Typography variant="body2" component="p">
                                        {item.description}
                                    </Typography>
                                    {props.Electronic ? (
                                            <Button size="medium"
                                                    color="primary"
                                                    href={item.replacementUrl}

                                            >
                                                {item.replacementUrl}
                                            </Button>) :
                                        null}
                                </CardContent>
                            )
                            : (<div>
                                    <TextField
                                        id="imageUrl"
                                        label="Bild URL"
                                        onChange={handleChange}
                                        style={{margin: 20}}
                                        placeholder="Placeholder"
                                        defaultValue={item.imageUrl}
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                    <TextField
                                        id="name"
                                        label="Name"
                                        onChange={handleChange}
                                        style={{margin: 20}}
                                        placeholder="Placeholder"
                                        defaultValue={item.name}
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                    <TextField
                                        id="description"
                                        label="Beschreibung"
                                        onChange={handleChange}
                                        multiline
                                        rowsMax="4"
                                        style={{margin: 20}}
                                        placeholder="Placeholder"
                                        defaultValue={item.description}
                                        enable={true}
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{shrink: true,}}
                                    />
                                    {props.Electronic ? (
                                            <TextField
                                                id="replacementUrl"
                                                label="Shop URL"
                                                onChange={handleChange}
                                                style={{margin: 20}}
                                                placeholder="Placeholder"
                                                defaultValue={item.replacementUrl}
                                                fullWidth
                                                margin="normal"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />) :
                                        null
                                    }
                                    {props.Electronic ? (
                                            <TextField
                                                id="UnitId"
                                                label="UnitId"
                                                type="number"
                                                minValue="0"
                                                onChange={handleChangeUnitId}
                                                style={{margin: 20}}
                                                placeholder="Placeholder"
                                                defaultValue={unitId}
                                                fullWidth
                                                margin="normal"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />) :
                                        null
                                    }

                                </div>
                            )
                        }
                        <CardActions>
                            {(isLoggedIn && !editMode) ? (
                                <IconButton onClick={() => setEditMode(true)} aria-label="settings">
                                    <CreateIcon />
                                </IconButton>) : null}
                            {(isLoggedIn && editMode) ? (
                                <IconButton onClick={() => save()} aria-label="settings">
                                    <SaveAltRoundedIcon />
                                </IconButton>) : null}
                            {props.Beverage && isLoggedIn && coins !== 0 ? (<h1>aufs Bild Klicken</h1>) : null}
                        </CardActions>

                    </div>) : null}

            </Card>

        </div>
    );
}

const mapStateToProps = state => (

    {
        Electronic: state.AppData.AppState.Electronic,
        Beverage: state.AppData.AppState.Beverage
    }
);

export default connect(mapStateToProps)(Item);
