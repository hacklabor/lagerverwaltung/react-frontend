import {server} from "./ProgrammSettings";
import {openSnackbar} from "./Notifier";
import {useState} from "react";
import {Post} from "./ServerDataHandling";
import Item from "./Item";

function PayBeverage(props) {
    const [payState1, setPayState1] = useState()
    const [payState2, setPayState2] = useState()


    const {UserId, ItemId} = props.match.params;

console.log("ITEMID",ItemId)
    console.log("UserID",UserId)
    if (parseInt(ItemId) > 1000000) {
        let txdata = {

            itemCount: 1,
            itemId: 1000000,
            storageUnitId: 1000000
        };
        (async () => {
            try {
                await Post("users/" + UserId + "/borrow", txdata);
                txdata = {

                    itemCount: 1,
                    itemId: ItemId,
                    storageUnitId: ItemId
                };
                await Post("users/" + UserId + "/borrow", txdata);
                let {history} = props;
                openSnackbar({message: 1 + ' Stk wurden entnommen'});
                history.push("/Blank");
                history.push("/BeverageList");


            } catch (e) {
                alert("Bezahlvorgang fehlgeschlagen: " + e);//...handle the error...

            }
        })();
    }


    if (parseInt(ItemId) === 1000000) {

        (async () => {
            let txdata = {

                itemCount: 4,
                itemId: 1000000,
                storageUnitId: 1000000
            };

            try {
              await  Post("users/" + UserId + "/supply", txdata);
                let {history} = props;
                openSnackbar({message: 4 + ' Neue Getränke Coins'});
                history.push("/Blank");
                history.push("/BeverageList");


            } catch (e) {
                alert("Aufladen fehlgeschlagen" + e);//...handle the error...

            }
        })();
    }
    return null

}

export default PayBeverage