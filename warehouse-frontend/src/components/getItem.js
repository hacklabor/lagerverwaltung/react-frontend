import React, {Component} from "react";

import Card from "@material-ui/core/Card";
import CardAction from "@material-ui/core/CardActions";

import Button from "@material-ui/core/Button";

import Item from "./Item"
import {connect} from "react-redux";
import {appStateAction} from '../action/appStateAction'
import Grid from "@material-ui/core/Grid";
import UnitShow from "./UnitShow";
import axios from "axios";
import {server} from "./ProgrammSettings";
import {ScanDataChange} from "../action/scanActions";
import {openSnackbar} from "./Notifier";
import {mqttSend} from "../action/mqttActions";
import {createData} from "./MqttSend";


// Or commonjs
let CountUnit = 0;

class getItem extends Component {
    getData = async (url) => {
        console.log("getItem_getData", url);
        let res = await axios.get(url);
        let {data} = res.data;
        return await data

    };


    postData = async (url, payload) => {
        console.log("getItem_postData", url, payload);
        let res = await axios.post(url, payload);
        let {data} = res.data;
        return await data;
    };

    constructor(props) {
        super(props);
        console.log("GetItem", props);
        this.state = {

            ItemID: props.match.params.ItemID,
            UnitID: props.match.params.UnitID,
            ItemData: null,
            updateCount: null,
            availableCount: 0,
            createUnit: null,
            UserData: null,
            flash: 0
        };

        if (!this.state.ItemData) {
            (async () => {
                try {
                    this.setState({ItemData: await this.getData(server() + "items/" + this.state.ItemID)});
                } catch (e) {
                    alert("GetItemData: " + e);//...handle the error...

                }
            })();
        }
    }

    ItemMinus(value) {

        if (CountUnit - value >= 0) {
            CountUnit -= value;
            (async () => {
                let txdata = {

                    itemCount: value,
                    itemId: this.props.match.params.ItemID,
                    storageUnitId: this.props.match.params.UnitID
                };
                this.setState({flash: 1});
                try {
                    this.setState({updateCount: await this.postData(server() + "users/" + this.props.UserID + "/borrow", txdata)});
                    let {history} = this.props;
                    openSnackbar({message: value + ' Stk wurden entnommen'});
                    history.push("/Blank");
                    history.push("/getItem/" + this.props.match.params.ItemID + "/" + this.props.match.params.UnitID)

                } catch (e) {
                    alert("GetItemData: " + e);//...handle the error...

                }
            })();
        }

    }

    ItemPlus(value) {


        (async () => {
            let txdata = {

                itemCount: value,
                itemId: this.props.match.params.ItemID,
                storageUnitId: this.props.match.params.UnitID
            };
            this.setState({flash: 1});
            try {
                this.setState({updateCount: await this.postData(server() + "users/" + this.props.UserID + "/supply", txdata)});
                let {history} = this.props;
                openSnackbar({message: value + ' Stk wurden eingelagert'});
                history.push("/Blank");
                history.push("/getItem/" + this.props.match.params.ItemID + "/" + this.props.match.params.UnitID)

            } catch (e) {
                alert("GetItemData: " + e);//...handle the error...

            }
        })();
    }

    save() {


    }


    componentDidMount() {
        if (!this.state.ItemData) {
            (async () => {
                try {
                    this.setState({ItemData: await this.getData(server() + "items/" + this.state.ItemID)});
                } catch (e) {
                    alert("GetItemData: " + e);//...handle the error...

                }
            })();
        }


        if (!this.state.UserData && this.props.login) {
            (async () => {
                try {
                    this.setState({UserData: await this.getData(server() + "users/" + this.props.UserID)});
                } catch (e) {
                    alert(e);//...handle the error...
                    console.log(e)
                }
            })();
        }
    }


    render() {


        if (this.state.createUnit) {


            (async () => {
                try {
                    this.setState({
                        createUnit: null,
                        ItemData: await this.getData(server() + "items/" + this.state.ItemID)
                    });
                } catch (e) {
                    alert("GetItemData: " + e);//...handle the error...

                }
            })();

            return <div>Loading</div>;
        }

        if (!this.state.ItemData) {
            return <div>Loading</div>;
        }

        if (!this.state.UserData && this.props.login) {
            return <div>Loading</div>;
        }


        let {UnitID, ItemID} = this.props.match.params;
        let i = 0;
        let UnitFound = 0;
        let CountAll = 0;
        let data = this.state.ItemData;
        let {storageUnits} = data;
        const {dispatch, login, scanMinus, scanPlus, history, lastUnitID} = this.props;

        if (storageUnits !== undefined) {
            storageUnits.sort((a, b) => a.id - b.id);
            if (storageUnits.length > 0) {
                if (storageUnits.length === 1 && parseInt(UnitID) !== parseInt(storageUnits[0].id)) {
                    history.push(history.push("/getItem/" + ItemID + "/" + storageUnits[0].id))
                }
                for (i = 0; i < storageUnits.length; i++) {
                    CountAll += storageUnits[i].availableCount;
                    if (parseInt(UnitID) === parseInt(storageUnits[i].id)) {
                        console.log("PI:", this.state.flash);
                        UnitFound = UnitID;
                        CountUnit = storageUnits[i].availableCount;
                        if (this.state.flash === 1) {
                            this.setState({flash: 0});
                            console.log("FLASH");
                            let PiLED = createData("flash", storageUnits[i].shelfId, storageUnits[i].row, storageUnits[i].column, UnitID);
                            dispatch(mqttSend(true, PiLED));
                        } else {
                            let PiLED = createData("location", storageUnits[i].shelfId, storageUnits[i].row, storageUnits[i].column, UnitID);
                            if (UnitID !== lastUnitID)
                                dispatch(mqttSend(true, PiLED));
                        }

                    }
                }
            }
        }


        if (UnitFound === 0 && UnitID > 0 && this.state.ItemData) {

            (async () => {
                try {


                    this.setState({
                        ItemData: null,
                        createUnit: await this.postData(server() + "storageUnits/" + UnitID + "/items", {
                            "itemId": ItemID
                        })
                    });

                } catch (e) {

                }
            })();
        }


        let Trigger = false;
        if (login === true && UnitID > 0) {
            Trigger = true;
            if (scanMinus > 0) {
                dispatch(ScanDataChange({minus: 0, plus: 0}));
                this.ItemMinus(scanMinus)
            }
            if (scanPlus > 0) {
                dispatch(ScanDataChange({minus: 0, plus: 0}));
                this.ItemPlus(scanPlus)
            }
        }

        dispatch(appStateAction(
            {
                lastPage: "GET_ITEM",
                lastURL: this.props.location.pathname,
                ItemId: this.state.ItemID,
                UnitID: UnitID,
                Electronic: true,
                Beverage: false
            }));

        return (

            <div>


                <Card style={{maxWidth: 500, margin: "auto",}}>


                    {Trigger ? (


                            <CardAction>


                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemMinus(10)}

                                >
                                    - 10
                                </Button>
                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemMinus(5)}

                                >
                                    - 5
                                </Button>
                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemMinus(1)}

                                >
                                    - 1
                                </Button>
                                <p/>


                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemPlus(1)}

                                >
                                    + 1
                                </Button>
                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemPlus(5)}

                                >
                                    + 5
                                </Button>
                                <Button
                                    size="large"
                                    color="secondary"
                                    variant="contained"
                                    onClick={() => this.ItemPlus(10)}

                                >
                                    + 10
                                </Button>


                            </CardAction>

                        ) :
                        login ? (


                                <CardAction>


                                    <h5>Keine UNIT Ausgewählt</h5>
                                </CardAction>) :
                            <CardAction>

                                <h5>Du bist nicht angemeldet</h5>


                            </CardAction>}

                    <Item
                        item={data}
                        isLogedIn={login}
                        unitID={UnitID}
                        count={CountAll}
                        user={this.state.UserData}
                        parrentProps={this.props}
                        large={true}

                    />
                </Card>

                {storageUnits ? (
                    <Grid
                        container
                        spacing={3}
                        style={{padding: 3, minWidth: 400, margin: "auto"}}
                        direction="column"
                        justify="center"
                        alignItems="center"
                    >

                        {storageUnits.map((currentItem, id) => (
                            <Grid key={id} item xs={12} sm={6} lg={6} xl={6}>
                                <UnitShow Unit={currentItem} props1={this.props} search={this.state.searchString}/>
                            </Grid>
                        ))}
                    </Grid>) : <p/>}

            </div>
        );
    }


}

const mapStateToProps = state => (

    {


        login: state.UserData.login,
        UserID: state.UserData.UserData.id,

        scanMinus: state.ScanData.scanData.minus,
        scanPlus: state.ScanData.scanData.plus,

        lastUnitID: state.AppData.AppState.UnitID,

    }
);

export default connect(mapStateToProps)(getItem)
