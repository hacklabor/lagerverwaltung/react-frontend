import axios from "axios";
import {server} from "./ProgrammSettings";
import {openSnackbar} from "./Notifier";


export const Put = async (url, payload, dbg, messageTxt) => {
    console.log("PutData: ", server() + url, payload, dbg);
    let res = await axios.put(server() + url, payload);
    let {data} = res.data;
    openSnackbar({message: messageTxt});
    return await data;
}

export const Post = async (url, payload, dbg, messageTxt) => {
    console.log("PostData: ", server() + url, payload, dbg);
    let res = await axios.post(server() + url, payload);
    let {data} = res.data;
    openSnackbar({message: messageTxt});
    return await data;
}

export const Get = async (url, dbgTxt, messageTxt) => {
    console.log("GetData: ", server() + url, dbgTxt);
    let res = await axios.get(server() + url);
    let {data} = res.data;
    //openSnackbar({message: messageTxt});
    return await data

};

export default Get