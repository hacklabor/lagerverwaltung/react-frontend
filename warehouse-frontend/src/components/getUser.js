import React, {useState} from "react";

import {connect} from "react-redux";
import {UserDataLogin} from '../action/userActions'

import {Link} from "react-router-dom";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {openSnackbar} from "./Notifier";
import TextField from "@material-ui/core/TextField";
import Item from "./Item";
import Get from "./ServerDataHandling";


// Or commonjs

function GetUser(props) {

    const [userData, setUserData] = useState()
    const [item, setItem] = useState()


    const [searchString, setSearchTxt] = useState("")

    const {User, lastURL} = props;
    const {rfidChipId} = User;
    if (!rfidChipId) {
        let history = props.history;
        history.push("/0");
    }


    if (!userData) {
        (async () => {
            try {
                setUserData(await Get("users/rfidChipId/" + rfidChipId, "GetUser"));
            } catch (e) {

                if (!rfidChipId || props.match.params.NEW === "0") {
                    let history = props.history;
                    openSnackbar({message: 'RFID ID nicht bekannt!'});
                    history.push("/0");
                } else {
                    let history = props.history;
                    history.push("/createUser/" + rfidChipId);
                }
            }
        })();
    }


    if (!userData) {
        return <div>Loading User</div>;
    }
    if (!item) {
        (async () => {
            try {
                setItem(await Get("items/", "GetItems getUser.js"));
            } catch (e) {
            }
        })();

    }
    if (!item) {
        return <div>Loading Items</div>;
    }

    const handleChange = event => {
        setSearchTxt(event.target.value)
    };

    console.log("UserData: ", userData)
    console.log("ItemData: ", item)
    let UserItem = [];
    let {inventory} = userData;
    let suchstring = searchString.toLowerCase();
    let i = 0;
    let anzahl = 0;


    for (i = 0; i < item.length; i++) {
        if ((parseInt(item[i].id) > 999999 && props.Beverage)
            || (parseInt(item[i].id) < 1000000 && props.Electronic)) {

            if (inventory[item[i].id] !== undefined) {
                if (searchString !== "") {
                    if (item[i].name.toLowerCase().indexOf(searchString.toLowerCase()) > -1
                        || item[i].description.toLowerCase().indexOf(searchString.toLowerCase()) > -1
                    ) {
                        anzahl += 1;
                        UserItem.push(item[i])
                    }
                } else {
                    UserItem.push(item[i]);
                    anzahl += 1;
                }
            }

        }
    }


    const {dispatch} = props;
    dispatch(UserDataLogin(userData, true));

    console.log("userItem", UserItem)
    if (props.Beverage){
  props. history.push("/BeverageList/")
    }
    return (
        <div>


            <h1> Benutzer: {userData.userName}</h1>
            <h3> Rfid_ID: {userData.rfidChipId}</h3>
            <Link to={lastURL} style={{textDecoration: 'none'}}>
                <Button
                    size="medium"
                    color="secondary"
                    variant="contained"
                    style={{margin: 10}}
                >

                    "Hier gehts weiter"
                </Button>
            </Link>
            <h2>Found: {anzahl} {"    "}</h2>

            <TextField
                style={{padding: 3}}
                id="searchInput"
                placeholder="Suche Bauteile"
                margin="normal"
                onChange={handleChange}
                value={searchString}
                inputProps={{
                    style: {fontSize: 40}
                }}
            />
            {UserItem ? (
                <Grid container spacing={3} style={{padding: 3}}>
                    {UserItem.map((currentItem) => (
                        <Grid key={currentItem.id} item xs={8} sm={3} lg={3} xl={2}>
                            <Item
                                item={currentItem}
                                isLogedIn={true}
                                unitID={0}
                                user={userData}
                                parrentProps={props}
                                search={searchString}
                            />
                        </Grid>
                    ))}
                </Grid>) : <p></p>}
        </div>
    )

}


const mapStateToProps = state => (

    {
        lastURL: state.AppData.AppState.lastURL,
        User: state.UserData.UserData,
        Electronic: state.AppData.AppState.Electronic,
        Beverage: state.AppData.AppState.Beverage
    }
);
export default connect(mapStateToProps)(GetUser);