import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardAction from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { openSnackbar } from './Notifier';
import TextField from "@material-ui/core/TextField";

import {connect} from "react-redux";

import {Link} from "react-router-dom";


// Or commonjs

class SetUnit2Item extends Component {
  constructor(props) {
    super(props);
      this.state = {
          UnitID: 0 }
    };

    link(ItemID) {


        let id1 = parseInt(this.state.UnitID);
        console.log (this.state.UnitID);
        if (Number.isInteger(id1) && id1>0 ) {
            let {history} = this.props;
            history.push("/createUnit/" + id1 + "/" + ItemID);
        }
            else {
                if (!Number.isInteger(id1)){
                    openSnackbar({ message: 'Eingabe ist keine Nummer' });

                }
                else{
                    openSnackbar({ message: 'Nummer muss größer als 0 sein' });
                }
            }

        }

    handleChangeUnitID = event => {
        this.setState({UnitID: event.target.value});
    };

  render() {



      const { ItemID } = this.props.match.params;


      return (
      <div>

          <Card>

            <CardContent>
              <TextField
                id="standard-full-with"
                label={    <Typography variant="" display="block" gutterBottom>Unit ID   </Typography>}
                onChange={this.handleChangeUnitID}
                style={{ margin: 20 }}
                placeholder="Unit ID eingeben"
                defaultValue=""
                fullWidth
                margin="normal"
                InputLabelProps={{
                  shrink: true
                }}
              />



            </CardContent>
            <CardAction>



              <Button
                variant="contained"
                size="medium"
                color="secondary"
                onClick={() => this.link(ItemID)}
              >
                zuordnen
              </Button>
                <Link to={"/getItem/"+ItemID} style={{ textDecoration: 'none' }}>
              <Button
                variant="contained"
                size="medium"
                color="primary"
                style={{ margin:5}}
              >
                zurück
              </Button>
                </Link>
            </CardAction>
          </Card>

      </div>
    );

}}
const mapStateToProps = state => (

    { retData: state.axios.items,
        loading: state.axios.loading,
        error: state.axios.error}
);

export default connect(mapStateToProps)(SetUnit2Item)

