
const baseUrl = process.env.REACT_APP_API_BASE_URL;
const PiBaseUrl = process.env.PI_LED_API_BASE_URL;

export const server=()=>{
   return `${baseUrl}`
};

export const PiServer=()=>{
   return "https://10.42.9.0:8082/"
};