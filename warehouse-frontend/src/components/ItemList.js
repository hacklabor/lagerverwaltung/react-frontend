import React, {useState} from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import {connect} from "react-redux";
import {appStateAction} from "../action/appStateAction";
import CardActions from '@material-ui/core/CardActions';
import Item from "./Item";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import AddBoxIcon from '@material-ui/icons/AddBox';
import {Get} from "./ServerDataHandling";
import {Link} from "react-router-dom";


function ItemList(props) {
    const {dispatch, IsLoggedIn, UserID} = props;
    const [isLoggedIn, setIsLoggedIn] = useState(IsLoggedIn);
    const [item, setItem] = useState(null);
    const [user, setUser] = useState(null);
    const [searchString, setSearchString] = useState("");
    const [nextItem, setNextItem] = useState(100);
    const [unitId, setUnitId] = useState(0);
    let NewItemList = [];
    let i = 0;

    let count = 0;
    let countDisplay = '';


    /********************************************************************
     * Funktionen
     ******************************************************************/

    const handleChange = event => {

        setSearchString(event.target.value)


    };

    function handleChangeUnitId(event) {
        let {value} = event.target;
        if (value < 0) {
            value = 0
        }
        setUnitId(value)
    }

    if (!item) {
        (async () => {
            try {
                setItem(await Get("items/"))
                //setItem(await Get("items/", "ItemList",""));
            } catch (e) {
                alert("Get_Item ItemList: " + e);//...handle the error...
            }
        })();
    }
    if (!user && isLoggedIn) {
        (async () => {
            try {
                setUser(await Get("users/" + UserID));
            } catch (e) {
                alert("Get_User ItemList: " + e);//...handle the error...
                console.log(e)
            }
        })();
    }


    if (!item) {
        return <div>Loading Items</div>;
    }
    if (!user && isLoggedIn) {
        return <div>Loading User</div>;
    }
    item.sort((a, b) => a.id - b.id);


    function newItem() {
        let lastValue = 0
        console.log("sortierte Liste",item)
        for (i = 0; i < item.length; i++) {
            console.log(i,Item.id,lastValue)
            if (item[i].id - lastValue > 2) {
                console.log("trigger",i,Item.id,lastValue)
              props.history.push("/createItem/"+(lastValue+1))
                break
            }
            lastValue=i
        }
    }


    dispatch(appStateAction(
        {
            lastPage: "ITEM_LIST",
            lastURL: props.location.pathname,
            ItemId: 0,
            UnitID: 0,
            Electronic: true,
            Beverage: false
        }));

    for (i = nextItem - 100; i < item.length; i++) {

        if (parseInt(item[i].id) < 1000000) {
            if (searchString !== "") {

                if (item[i].name.toLowerCase().indexOf(searchString.toLowerCase()) > -1
                    || item[i].description.toLowerCase().indexOf(searchString.toLowerCase()) > -1
                ) {
                    NewItemList.push(item[i]);
                    count += 1;
                }

            } else {
                NewItemList.push(item[i]);
                count += 1;
            }
            if (count > 99) {
                break
            }
        }
    }
    let NextButton = true;
    let prevButton = true;
    if (count < 100) {
        NextButton = false
    }
    if (nextItem < 101) {
        prevButton = false
    }
    if (count > 1) {
        countDisplay = count + ' Bauteile gefunden';
    }
    NewItemList.sort((a, b) => a.id - b.id);


    return (
        <div>

            {NewItemList ? (
                <div>
                    <Grid container spacing={3}>
                        <Grid item spacing={2} style={{padding: 30}} xs={12} sm={6}>
                            <Card>
                                <CardContent>
                                    <TextField
                                        style={{padding: 5}}
                                        id="searchInput"
                                        placeholder="Suche Bauteile"
                                        margin="none"
                                        fullWidth
                                        inputProps={{
                                            style: {fontSize: 40}
                                        }}
                                        style={{margin: 20, color: "White"}}
                                        onChange={handleChange}
                                        value={searchString}
                                    />
                                    <Typography variant="h6" component="p" style={{margin: 20}}>
                                        {countDisplay}
                                    </Typography>
                                    <TextField
                                        id="UnitId"
                                        label="UintId"
                                        type="number"
                                        minValue="0"
                                        onChange={handleChangeUnitId}
                                        style={{margin: 20, color: "White"}}
                                        placeholder="Box Nummer"
                                        defaultValue={unitId}
                                        inputProps={{
                                            style: {fontSize: 40}
                                        }}
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={unitId}
                                    />
                                </CardContent>
                                <CardActions>
                                    <Link to={"/getUnit/" + unitId} style={{textDecoration: "none"}}>
                                        <Button
                                            size="large"
                                            color="secondary"
                                            variant="contained">
                                            Box suchen
                                        </Button>
                                    </Link>
                                </CardActions>


                            </Card>

                        </Grid>
                    </Grid>


                    {prevButton ? (<Button
                        variant="contained"
                        size="medium"
                        color="secondary"
                        style={{margin: 5}}
                        onClick={() => {
                            if (nextItem > 199) {
                                setNextItem(nextItem - 100)
                            }
                        }}
                    >
                        weniger
                    </Button>) : null}
                    {NextButton ? (<Button
                        variant="contained"
                        size="medium"
                        color="secondary"
                        style={{margin: 5}}
                        onClick={() => {
                            setNextItem(nextItem + 100)
                        }}
                    >
                        mehr
                    </Button>) : <p></p>}


                    <Grid container spacing={3} style={{padding: 3}}>

                        {isLoggedIn ? (<Card style={{maxWidth: 250}}>

                                <CardContent>
                                    <AddBoxIcon onClick={()=>newItem()} style={{
                                        width: 200,
                                        height: 200, margin: "auto"
                                    }}/>


                                </CardContent>

                        </Card>) : null}


                        {NewItemList.map((currentItem) => (
                            <Grid key={currentItem.id} item xs={8} sm={3} lg={3} xl={2}>

                                <Item
                                    item={currentItem}
                                    isLogedIn={isLoggedIn}
                                    unitID={0}
                                    user={user}
                                    parrentProps={props}
                                    search={searchString}
                                    coins={0}
                                    large={false}
                                />

                            </Grid>
                        ))}
                    </Grid>
                </div>
            ) : (
                "No Items found"
            )}
        </div>
    );
}

const mapStateToProps = state => (
    {
        UserID: state.UserData.UserData.id,
        IsLoggedIn: state.UserData.login
    }

);

export default connect(mapStateToProps)(ItemList)
