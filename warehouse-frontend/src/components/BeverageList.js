import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import axios from 'axios'
import {connect} from "react-redux";
import {appStateAction} from "../action/appStateAction";
import {server} from "./ProgrammSettings";
import Item from "./Item";



class BeverageList extends Component {
    getData = async (url) => {
        let res = await axios.get(url);
        let {data} = res.data;
        return await data

    };


    constructor(props) {
        super();
        this.state = {

            ItemData: null,
            UserData1: null,
            searchString: "",
            NextBauteile: 100

        };


    }

    handleChangeSuche = event => {
        this.setState({

            searchString: event.target.value,
            NextBauteile: 100
        })
    };


    componentDidMount() {
        let {login, UserID} = this.props;
        if (!this.state.ItemData) {
            (async () => {
                try {
                    this.setState({ItemData: await this.getData(server() + "items/")});
                } catch (e) {
                    alert("GetItemListData: " + e);//...handle the error...

                }
            })();
        }
        if (login) {
            (async () => {
                try {
                    this.setState({UserData1: await this.getData(server() + "users/" + UserID)});
                } catch (e) {
                    alert("GetItemListData: " + e);//...handle the error...

                }
            })();
        } else {
            this.setState({UserData1: []})
        }


    }


    render() {
        let coins = 0;
        let coinItem = {}
        if (!this.state.ItemData) {
            return <div>Loading</div>;
        }
        if (!this.state.UserData1) {
            return <div>Loading</div>;
        }
        let {inventory} = this.state.UserData1;

        console.log("USERDATA", this.state.UserData1);
        console.log("Inventory", inventory, "Tooken", coins)
        if (inventory) {
            if (inventory[1000000]) {
                coins = inventory[1000000].count;
                console.log("USERDATA", this.state.UserData1);
                console.log("Inventory", inventory, "Tooken", coins)
            }
        }


        let {ItemData} = this.state;

        let NewItemList = [];
        let i = 0;
        let suchstring = this.state.searchString.toLowerCase();
        let anzahl = 0;
        let countDisplay = '';
        let ueberschrift = 'Getränkeverwaltung';
        const {dispatch, login} = this.props;
        if (login === false) {
            ueberschrift += ' Log dich ein!'
        }
        console.log("USERDATA", this.state.UserData1);
        dispatch(appStateAction(
            {
                lastPage: "BEVERAGE_LIST",
                lastURL: this.props.location.pathname,
                ItemId: 0,
                UnitID: 0,
                Electronic: false,
                Beverage: true
            }));

        for (i = this.state.NextBauteile - 100; i < this.state.ItemData.length; i++) {
            if (parseInt(this.state.ItemData[i].id) === 1000000) {coinItem=this.state.ItemData[i]}
            if (parseInt(this.state.ItemData[i].id) > 1000000) {

                if (suchstring !== "") {

                    if (this.state.ItemData[i].name.toLowerCase().indexOf(suchstring) > -1
                        || this.state.ItemData[i].description.toLowerCase().indexOf(suchstring) > -1
                    ) {
                        NewItemList.push(this.state.ItemData[i]);
                        anzahl += 1;
                    }

                } else {
                    NewItemList.push(this.state.ItemData[i]);
                    anzahl += 1;
                }
                if (anzahl > 99) {
                    break
                }
            }
        }
        let NextButton = true;
        let prevButton = true;
        if (anzahl < 100) {
            NextButton = false
        }
        if (this.state.NextBauteile < 101) {
            prevButton = false
        }
        if (anzahl > 1) {
            countDisplay = anzahl - 1 + ' Getränke gefunden';
        }
        NewItemList.sort((a, b) => a.id - b.id);
        return (
            <div>
                <h1>{ueberschrift}</h1>

                <div>
                    <Grid container spacing={3}>
                        <Grid item spacing={2} style={{padding: 30}} xs={12} sm={6}>
                            <TextField
                                style={{padding: 5}}
                                id="searchInput"
                                placeholder="Suche Getränke"
                                margin="none"
                                fullWidth
                                inputProps={{
                                    style: {fontSize: 40}
                                }}
                                onChange={this.handleChangeSuche}
                            />

                            <Typography variant="h6" component="p">
                                {countDisplay}
                            </Typography>
                        </Grid>
                    </Grid>


                    {prevButton ? (<Button
                        variant="contained"
                        size="medium"
                        color="secondary"
                        style={{margin: 5}}
                        onClick={() => {
                            if (this.state.NextBauteile > 199) {
                                this.setState({NextBauteile: this.state.NextBauteile - 100})
                            }
                        }}
                    >
                        weniger
                    </Button>) : null}
                    {NextButton ? (<Button
                        variant="contained"
                        size="medium"
                        color="secondary"
                        style={{margin: 5}}
                        onClick={() => {
                            this.setState({NextBauteile: this.state.NextBauteile + 100})
                        }}
                    >
                        mehr
                    </Button>) : <p></p>}

                    <Item
                        item={coinItem}
                        isLogedIn={this.props.login}
                        unitID={0}
                        user={this.state.UserData1}
                        parrentProps={null}
                        search={""}
                    />


                    {NewItemList ? (

                    <Grid container spacing={3} style={{padding: 3}}>
                        {NewItemList.map((currentItem, id) => (
                            <Grid key={id} item xs={8} sm={3} lg={3} xl={2}>


                                <Item
                                    item={currentItem}
                                    isLogedIn={this.props.login}
                                    unitID={0}
                                    user={this.state.UserData1}
                                    parrentProps={null}
                                    search={""}
                                    coins={coins}
                                    large={true}
                                />
                                {/*<Beverage Item1={currentItem} props1={this.props} coins={coins} search={this.state.searchString}/>*/}
                            </Grid>
                        ))}
                    </Grid>

                ) : (
                "No Items found"
                )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => (

    {
        login: state.UserData.login,
        UserID: state.UserData.UserData.id,

    }
);

export default connect(mapStateToProps)(BeverageList)
