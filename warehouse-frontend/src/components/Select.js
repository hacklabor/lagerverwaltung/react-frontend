import React, {Component} from "react";

import {connect} from "react-redux";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import unitImage from "../images/unit1.jpg"
import bevImage from "../images/beverage1.jpg"

import {Link} from "react-router-dom";


class BeverageList extends Component {





    render() {

        return (
            <div>

                <Grid container spacing={4} justify="center" align="center" alignItems="stretch" style={{paddingTop: 30}}>

                    <Grid item xs={10} sm={6} md={4} lg={3} xl={2} align="left">
                        <Link to={"/ItemList/"} style={{textDecoration:"none"}}>
                            <Card>
                                <CardActionArea>
                                    <CardMedia
                                        style={{height: 200}}
                                        image={unitImage}
                                        titel="Elektronik Lager"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Elektronik Lager
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            Im Elektronik Lager kannst du Bauteile ausleihen oder anderen Mitgliedern zur Verfügung stellen.
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" color="primary">
                                            Gehe zum Elektronik Lager
                                        </Button>
                                    </CardActions>
                                </CardActionArea>
                            </Card>
                        </Link>
                    </Grid>

                    <Grid item xs={10} sm={6} md={4} lg={3} xl={2} align="left">
                        <Link to={"/BeverageList/"} style={{textDecoration:"none"}}>
                            <Card style={{ height: '100%' }}>
                                <CardActionArea>
                                    <CardMedia
                                        style={{height: 200}}
                                        image={bevImage}
                                        titel="Getränke Lager"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Getränke Lager
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            Im Getränke Lager kannst du einlagern.<br /><br />
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" color="primary">
                                            Gehe zum Getränke Lager
                                        </Button>
                                    </CardActions>
                                </CardActionArea>
                            </Card>
                        </Link>
                    </Grid>

                </Grid>
            </div>

        );
    }
}

const mapStateToProps = state => (

    {login: state.UserData.login}
);

export default connect(mapStateToProps)(BeverageList)
