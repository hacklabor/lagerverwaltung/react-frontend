
import React from 'react';
import {connect} from "react-redux";
import {mqttSend} from "../action/mqttActions";


export const createData = (Typ,ShelfNr,RowNr,CollNr,UnitID)=>{
    let PiLED = {
        "function": Typ,
        "row": RowNr- 1,
        "shelf": ShelfNr - 1,
        "column": CollNr- 1,
        "size": UnitSize(UnitID)
    };

    return PiLED
};

export const UnitSize=(UnitID)=>{

    if (UnitID>299){
        return "small"
    }
    else{
        return "large"
    }

};

class MqttSend extends React.Component {

  render(){
    const {mqtt,sendMqtt,payloadMqtt,dispatch} = this.props;


      if (sendMqtt===true){
         console.log(mqtt,"Message",payloadMqtt,sendMqtt);
         mqtt.publish('WarehouseLights', JSON.stringify(payloadMqtt));
         dispatch(mqttSend(false, "0"));
      }

    return (
        <div>

        </div>
    )

  }
}
const mapStateToProps = state => (

    {


      sendMqtt: state.MQTT.sendData,
      payloadMqtt: state.MQTT.payload




    }
);

export default connect(mapStateToProps)(MqttSend)