import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import {server} from "./ProgrammSettings"
import axios from "axios";
import CardActions from "@material-ui/core/CardActions";
import {CardContent} from "@material-ui/core";

// Or commonjs

class CreateUser extends Component {
  getData = async (url) => {
    console.log("getData",url);
    let res = await axios.get(url);
    let { data } = res.data;
    return await data

  };
  constructor(props) {
    super(props);
    this.state = {
      Status: 500,
UserData: null,
        name: "Hier steht dein Name",
      UsernameValid:true

    };

    console.log(props);
    console.log("CreateUSER")
  }

  componentDidMount() {
    console.log("DidMount: createUser",this.props);

    if (!this.state.UserData) {
      (async () => {
        try {
          this.setState({UserData: await this.getData(server()+"users/")}); //später user

        } catch (e) {
             }
      })();
    }
    }


  handleChangeName = event => {
    this.setState({ name: event.target.value });
    this.setState({ UsernameValid: true});
 let i=0;
    for (i=0; i<this.state.UserData.length;i++){

      if (event.target.value ===this.state.UserData[i].userName){
        this.setState({ UsernameValid: false});

      }

    }

  };

  render() {

    if (!this.state.UserData){
      if (!this.state.UserData){
        return <div>Loading User</div>;
      }
    }

    let UsernameValid =this.state.UsernameValid;


    return (

      <div>
        {UsernameValid ? (
            <CardActions>
            <Typography variant="h4" gutterBottom>
              User anlegen {this.props.match.params.ID}
            </Typography>
              <CardContent>

            <TextField
               style={{backgroundColor: "lime"}}
            id="Name"
          name="Name"
          label="Name"
          fullWidth
          autoComplete="Name"
          onChange={this.handleChangeName} />
          <button
          className="btn btn-secondary btn-sm"
          onClick={() => this.post()}
          >
          Benutzer anlegen
          </button>
          <h1>{this.state.Status}</h1>
          <h5>{JSON.stringify(this.state.jsonDATA)}</h5>
              </CardContent>
            </CardActions> ):

            <CardActions>
              <Typography variant="h4" gutterBottom>
                User anlegen {this.props.match.params.ID}
              </Typography>

<CardContent>

              <TextField
                  style={{backgroundColor: "red"}}
                  id="Name"
                  name="Name"
                  label="Name"
                  fullWidth
                  autoComplete="Name"
                  onChange={this.handleChangeName} />
                           <h1>{this.state.Status}</h1>
              <h5>{JSON.stringify(this.state.jsonDATA)}</h5>
</CardContent>
            </CardActions>
        }
      </div>
    );
  }

  post() {
    let TxData = {
      id: this.props.match.params.ID,
      rfidChipId: this.props.match.params.ID,
      userName: this.state.name

    };

    console.log(TxData);
    axios({
      method: "post",
      url: server()+"users",
      data: TxData
    }).then(res => {
      console.log(res.status);
      this.setState({
        Status: res.status,
        jsonDataRx: res.data
      });
    });
  }
   
}

export default CreateUser;
