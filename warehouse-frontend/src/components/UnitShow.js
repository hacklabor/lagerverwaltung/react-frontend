import React, {useState} from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {Link} from 'react-router-dom';
import {CardActions} from "@material-ui/core";
import {connect} from "react-redux";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import IconButton from "@material-ui/core/IconButton";
import CreateIcon from "@material-ui/icons/Create";
import SaveAltRoundedIcon from "@material-ui/icons/SaveAltRounded";
import Get, {Put} from "./ServerDataHandling";
import TextField from "@material-ui/core/TextField";


function UnitShow(props) {


    const [UnitData, setUnitData] = useState()
    let col = "white";
    let ids = props.props1.match.params;
    let {isLoggedIn} = props;
    let link1 = "/getItem/" + ids.ItemID + "/" + props.Unit.id;
    const [editMode, setEditMode] = useState(false);


    if (!UnitData) {
        (async () => {
            try {
                setUnitData(await Get("storageUnits/" + props.Unit.id));
            } catch (e) {
                alert(e);//...handle the error...
                console.log(e)
            }
        })();
    }

    console.log("UnitData", UnitData)


    function handleChange(event) {
        let {id, value} = event.target;
        if (value<0){value=0}

        setUnitData((prevValue) => {
            return {
                ...prevValue,
                [id]: value,
            };
        });
    }

    if (!UnitData) {
        return null
    }
    if (parseInt(UnitData.id) === parseInt(ids.UnitID)) {
        console.log(UnitData);

        col = "lime"
    }

    function save() {
        setEditMode(false);
        let Id = [];
        let i
        for (i = 0; i < UnitData.items.length; i++) {
            Id.push(UnitData.items[i].id)

        }

        (async () => {
            try {
                await Put(
                    "storageUnits/" + UnitData.id,
                    {
                        column: UnitData.column,
                        itemIds: Id,
                        row: UnitData.row,
                        shelfId: UnitData.shelfId
                    },
                    "Put Unit Data",
                    "Lagerzuordnung geändert")
            } catch (e) {
                alert("Put Unit Data " + e);//...handle the error...

            }
        })();

    }


    return (

        <div>

            {UnitData ? (

                <Card style={{maxWidth: 400, backgroundColor: col, margin: "auto"}}>
                    <Link to={link1} style={{textDecoration: "none"}}>
                        <CardContent>


                            <Typography variant="h6" component="h6">Anzahl: {props.Unit.availableCount}  </Typography>

                            <Typography variant="body2" color="textSecondary" component="p">Unit
                                ID: {UnitData.id}</Typography>
                            {editMode ? (
                                    <div>
                                        <TextField
                                            id="shelfId"
                                            label="shelfId"
                                            type="number"
                                            minValue="0"
                                            onChange={handleChange}
                                            style={{margin: 20}}
                                            placeholder="LagerNr"
                                            defaultValue={UnitData.shelfId}

                                            margin="normal"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            value={UnitData.shelfId}
                                        />
                                        <TextField
                                            id="row"
                                            label="Reihe"
                                            type="number"
                                            minValue="0"
                                            onChange={handleChange}
                                            style={{margin: 20}}
                                            placeholder="Lagerreihe"
                                            defaultValue={UnitData.row}

                                            margin="normal"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            value={UnitData.row}
                                        />
                                        <TextField
                                            id="column"
                                            label="Spalte"
                                            type="number"
                                            minValue="0"
                                            onChange={handleChange}
                                            style={{margin: 20}}
                                            placeholder="LagerSpalte"
                                            defaultValue={UnitData.column}

                                            margin="normal"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            value={UnitData.column}
                                        />
                                    </div>

                                ) :
                                <Typography variant="body2" color="textSecondary"
                                            component="p">Regal: {UnitData.shelfId} Reihe: {UnitData.row} Spalte: {UnitData.column}

                                </Typography>}
                        </CardContent>
                    </Link>


                    {isLoggedIn ? (
                        <CardActions>

                            {!editMode ? (
                                <IconButton aria-label="settings" onClick={() => setEditMode(true)}>
                                    <CreateIcon style={{margin: "left"}} />
                                </IconButton>) : null}
                            {editMode ? (
                                <IconButton aria-label="settings"  onClick={() => save()}>
                                    <SaveAltRoundedIcon style={{margin: "left"}}/>
                                </IconButton>) : null}

                            {!props.Unit.availableCount ? (
                                <Link to={"/deleteItem2Unit/" + ids.ItemID + "/" + UnitData.id}
                                      style={{textDecoration: "none"}}>
                                    <DeleteForeverIcon style={{margin: "right"}}/>
                                </Link>
                            ) : null}
                        </CardActions>
                    ) : null}


                </Card>

            ) : <h1>test133</h1>}

        </div>
    )


}

const mapStateToProps = state => (

    {


        isLoggedIn: state.UserData.login,


    }
);

export default connect(mapStateToProps)(UnitShow)

