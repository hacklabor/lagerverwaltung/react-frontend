import React, {Component} from "react";

import "./App.css";
import {Route, Switch} from 'react-router-dom'
//components
import NavBar from "./components/NavBar";
import ItemList from "./components/ItemList";
import getItem from "./components/getItem";

import getUnit from "./components/getUnit";
import GetUser from "./components/getUser";
import createUser from "./components/createUser";
import createItem from "./components/createItem"
import SelectWarehouse from "./components/Select"
import Blank from "./components/blank"
import {connect} from "react-redux";
import {UserDataLogin} from "./action/userActions";
import DeleteItem2Unit from "./components/deleteItem2Unit";
import setUnit2Item from "./components/setUnit2Item";
import {Connector, subscribe} from 'react-mqtt';
import _MqttSend from './components/MqttSend.js';
import BeverageList from "./components/BeverageList";
import PayBeverage from "./components/getraenkeZahlen";
import ImportItemsforDebug from "./components/copyItemsforDebug";


const MqttSend = subscribe({topic: 'WarehouseLights'})(_MqttSend);

class App extends Component {


    constructor(props) {
        super(props);
        this.state = {

            unitID: 0,
            StartTime: 0,
            LoginTime: 0,
            HM_trigger: false


        };
    }


    onKeyDown = (event) => {
        const {lastPage, lastItemID, lastURL, login, Electronic} = this.props;

        let id;
        let history = this.props.history;


        // Sobald "CR"
        if (event.keyCode === 13 && Electronic === true) {
            const {dispatch} = this.props;

            let timespan = (new Date().getTime() - this.state.StartTime);

            if (this.text.length > 0) {
                console.log("INPUT_TEXT", this.text);
                /*   if (this.text.indexOf("scanplus") > -1 && lastPage === "GET_ITEM" && login === true) {
                      let wert = this.text.split("plus");


                      dispatch(ScanDataChange({minus: 0, plus: wert[wert.length - 1]}));
                      history.push("/Blank/");
                      history.push(lastURL);
                  }
                  if (this.text.indexOf("scanminus") > -1 && lastPage === "GET_ITEM" && login === true) {
                      let wert = this.text.split("minus");
                      dispatch(ScanDataChange({minus: wert[wert.length - 1], plus: 0}));
                      history.push("/Blank/");
                      history.push(lastURL);
                  }
                  if (this.text.indexOf("setunit") > -1 && lastPage === "GET_ITEM") {
                      id = this.text.split("setunit");

                      history.push("/getUnit/" + lastItemID + "/" + id[id.length - 1]);
                      this.setState({
                          unitID: id[id.length - 1],
                          StartTime: new Date().getTime()
                      })
                 }*/

                //Unit?

                if (this.text.indexOf("UnitShift/") > -1) {
                    id = this.text.split("UnitShift/");
                    console.log("Unit TEXT");
                    //history.push("/createUnit/" + id[id.length - 1] + "/-1");


                    this.setState({
                        unitID: id[id.length - 1],
                        StartTime: new Date().getTime()
                    })
                }

                if (this.text.indexOf("shelf") > -1) {
                    console.log(timespan);
                    if (timespan < 60000) {

                        let id;
                        id = this.text.split("shelf");

                        let history = this.props.history;

                        history.push("/Blank");
                        let locId = id[id.length - 1].split(",").join("/");
                        console.log("sehlf", locId);
                        this.setState({

                            StartTime: 0
                        });
                        history.push("/getUnit/" + this.state.unitID + "/" + locId)
                    }
                }
                // 10 Zeichen vielleicht rfidID
                if (this.text.length >= 10) {

                    let id1 = parseInt(this.text.substring(this.text.length - 10, this.text.length + 10));
                    // Number?
                    if (Number.isInteger(id1)) {

                        dispatch(UserDataLogin({rfidChipId: id1}, false));
                        history.push("/getUser/" + id1);
                    }
                }
            }
            this.text = ""
        } else {
            this.text += event.key;
        }
    };

    componentDidMount() {
        document.addEventListener("keydown", this.onKeyDown.bind(this));
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.onKeyDown.bind(this));
    }


    render() {
        console.log("APPRENDER");
        let {Electronic, Beverage} = this.props;
        return (

            <Connector mqttProps="wss://warehouse01.hacklabor.de:1884">
                <div style={{backgroundColor: "#222831",}}>
                    <Route path="/" component={NavBar}/>

                    <Switch>


                        <Route path="/Blank" component={Blank}/>
                        <Route path="/getItem/:ItemID/:UnitID" component={getItem}/>
                        <Route path="/setUnit2Item/:ItemID" component={setUnit2Item}/>
                        <Route path="/deleteItem2Unit/:ItemId/:UnitId" component={DeleteItem2Unit}/>
                        <Route path="/getUnit/:UnitID/:ShelfNr/:RowNr/:ColNr" component={getUnit}/>
                        <Route path="/getUnit/:UnitID" component={getUnit}/>
                        <Route path="/createUser/:ID" component={createUser}/>
                        <Route path="/getUser/:NEW" component={GetUser}/>
                        <Route path="/copy/item" component={ImportItemsforDebug}/>
                        <Route exact path="/createItem/:NextId" component={createItem}/>

                        <Route path="/ItemList" component={ItemList}/>
                        <Route path="/BeverageList" component={BeverageList}/>
                        <Route path="/PayBeverage/:UserId/:ItemId" component={PayBeverage}/>
                        {Electronic ? (<Route path="/" component={ItemList}/>) :
                            Beverage ? (<Route path="/" component={BeverageList}/>) :
                                <Route path="/" component={SelectWarehouse}/>
                        }


                    </Switch>
                    <MqttSend/>

                </div>
            </Connector>

        );
    }

}


const mapStateToProps = state => (

    {
        User: state.UserData.UserData,
        login: state.UserData.login,
        lastPage: state.AppData.AppState.lastPage,
        lastItemID: state.AppData.AppState.ItemId,
        lastURL: state.AppData.AppState.lastURL,
        Electronic: state.AppData.AppState.Electronic,
        Beverage: state.AppData.AppState.Beverage

    }
);
export default connect(mapStateToProps)(App);





